<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductCreateRequest;
use App\ProductRepositoryInterface as ProductRepository;
use App\Services\Product as ProductService;

class ProductController extends Controller
{
	protected $productRepository;
    protected $productService;
     
  	public function __construct(ProductRepository $productRepository, ProductService $productService)
  	{
        $this->productRepository = $productRepository;
        $this->productService = $productService;
  	}

    public function index(){
    	return view('product.index', [
    		'products' => $this->productRepository->all()
    	]);
    }

    public function edit($id){
        return view('product.edit', [
            'product' => $this->productRepository->find($id)
        ]);
    }

    public function update(ProductRequest $request, $id){
        $data = $request->only([
            'name', 
            'description', 
            'price', 
            'category', 
        ]);
        $data['free_shipping'] = $request->get('free_shipping', 0);
        $this->productService->update($data, $id);
        return redirect()->route('product.index')->with('success', 'Product has been updated.');
    }

    public function destroy($id){
        $this->productService->destroy($id);
        return redirect()->route('product.index')->with('success', 'Product has been removed.');
    }

    public function create(){
        return view('product.create');
    }

    public function import(ProductCreateRequest $request){
        try{
            $this->productService->upload($request->file('file'));
            return redirect()->route('product.index')->with('success', 'File has been sent to queue.');
        }catch (\Exception $e){
            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
