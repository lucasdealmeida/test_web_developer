<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;

class Product extends Model
{
    use PresentableTrait;

    protected $presenter = \App\ProductPresenter::class;

    protected $fillable = ['lm', 'name', 'description', 'free_shipping', 'price', 'category'];
}
