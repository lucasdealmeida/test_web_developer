<?php 
namespace App;
  
use App\ProductRepositoryInterface;
use App\Product;
 
class ProductEloquentRepository implements ProductRepositoryInterface{
 
    public function findByLm($lm){
        return Product::where('lm', $lm)->first();
    }

    public function all()
    {
        return Product::all();
    }

    public function create($data){
        return Product::create($data);
    }
 
    public function find($id)
    {
        return Product::find($id);
    }
 
    public function update($data, $id)
    {
        $model = Product::findOrFail($id);
        $model->forceFill($data);
        return $model->save();
    }

    public function destroy($id)
    {
        return Product::destroy($id);
    }
}