<?php

namespace App;

use Excel;

class ProductFile{

	protected $products = [];

    public function validateFile($file_name){
        $this->readFile($file_name, false, true);
    }    

    public function getProducts($file_name){
        $this->readFile($file_name, false, true);
        return $this->products;
    }

    protected function readFile($file_name, $validate = false, $products = false){
        Excel::selectSheetsByIndex(0)->load($file_name, function($sheet) use ($validate, $products){
            $sheet->noHeading();
            $sheet->ignoreEmpty();

            $category = $sheet->limit(1, 1)->get()[0];
            $category_name = $category[1];
            if ($validate){
                $this->validateCell($category[0], 'category');

                $heading = $sheet->limit(1, 3)->get()[0];
                $this->validateCell($heading[0], 'lm');
                $this->validateCell($heading[1], 'name');
                $this->validateCell($heading[2], 'free_shipping');
                $this->validateCell($heading[3], 'description');
                $this->validateCell($heading[4], 'price');
            }

            if ($products){
                $sheet->limit(null, 4)->get()->each(function($row) use ($category_name){
                    $this->setProduct([
                        'lm' => $row[0],
                        'name' => $row[1],
                        'free_shipping' => $row[2],
                        'description' => $row[3],
                        'price' => $row[4],
                        'category' => $category_name,
                    ]);
                });
            }
        });
    }

	protected function setProduct($product){
		$this->products[] = $product;
	}

	protected function validateCell($cell, $value){
		if (strtolower($cell) != $value){
			throw new \Exception(ucfirst($value) . " not found in file.");
		}
	}

}