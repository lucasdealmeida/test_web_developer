<?php
namespace App;

use Laracasts\Presenter\Presenter;

class ProductPresenter extends Presenter {

    public function getPrice()
    {
        return '$' . number_format($this->price, 2);
    }

    public function getFreeShipping()
    {
        return ($this->free_shipping) ? 'Yes' : 'No';
    }

}