<?php

namespace App;

interface ProductRepositoryInterface{
 
    public function all();

    public function create($data);
 
    public function find($id);
 
    public function update($data, $id);

    public function destroy($id);
 
}