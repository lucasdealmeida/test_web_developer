<?php

namespace App\Services;

use App\ProductRepositoryInterface as ProductRepository;
use App\ProductFile as ProductFile;
use Illuminate\Http\Request;
use App\Jobs\ImportProducts;

class Product {

	protected $producRepository;

	protected $productFile;

	public function __construct(ProductRepository $producRepository, ProductFile $productFile){
		$this->producRepository = $producRepository;
		$this->productFile = $productFile;
	}

	public function update($data, $id){
		return $this->producRepository->update($data, $id);
	}

	public function destroy($id){
		return $this->producRepository->destroy($id);	
	}

	public function import($file){
		$products = $this->productFile->getProducts($file);
        foreach ($products as $data){
        	if ($product = $this->producRepository->findByLm($data['lm'])){
        		$product->name = $data['name'];
        		$product->description = $data['description'];
        		$product->price = $data['price'];
        		$product->free_shipping = $data['free_shipping'];
        		$product->category = $data['category'];
        	}else{
        		$this->producRepository->create($data);
        	}
        }
	}

	public function upload($file){
		$this->productFile->validateFile($file->path());
        $path = storage_path('app/' . $file->store('products', 'local'));
        dispatch(new ImportProducts($path));
	}

}