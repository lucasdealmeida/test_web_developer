@extends('layout')	

@section('content')

<form action="{{ route('product.import') }}" method="POST" enctype="multipart/form-data">
	<h2>Import Products</h2>
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	{!! csrf_field() !!}
	<div class="form-group">
		<labe>File</labe>
		<input type="file" name="file">
	</div>

	<a class="btn btn-default" href="{{ route('product.index') }}">Cancel</a>
	<button class="btn btn-success" type="submit">Save</button>

</form>
@endsection