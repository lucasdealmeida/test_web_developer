@extends('layout')	

@section('content')

<form action="{{ route('product.update', ['id' => $product->id]) }}" method="POST">
	<h2>Product #{{ $product->lm }}</h2>
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	{!! csrf_field() !!}
	<div class="form-group">
		<labe>Name:</labe>
		<input type="text" class="form-control" value="{{ $product->name }}" name="name">
	</div>

	<div class="form-group">
		<labe>Category:</labe>
		<input type="text" class="form-control" value="{{ $product->category }}" name="category">
	</div>

	<div class="form-group">
		<labe>Description:</labe>
		<textarea name="description" class="form-control" cols="20" rows="10">{{ $product->description }}</textarea>
	</div>

	<div class="form-group">
		<labe>Price:</labe>
		<input type="text" class="form-control" value="{{ $product->price }}" name="price">
	</div>

	<div class="checkbox">
    	<label>
      		<input type="checkbox" value="1" name="free_shipping" {{ ($product->free_shipping) ? 'checked="checked"' : '' }}> Free Shipping
    	</label>
  	</div>

	<a class="btn btn-default" href="{{ route('product.index') }}">Cancel</a>
	<button class="btn btn-success" type="submit">Save</button>

</form>
@endsection