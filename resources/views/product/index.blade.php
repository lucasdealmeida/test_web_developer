@extends('layout')	

@section('content')
<a class="btn btn-success" href="{{ route('product.create') }}">Import Products</a>
<hr>
@if ($products)
	<table class="table table-striped">
		<thead>
			<tr>
				<th>lm</th>
				<th>Name</th>
				<th>Category</th>
				<th>Free Shipping</th>
				<th>Description</th>
				<th>Price</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($products as $product)
				<tr>
					<td>{{ $product->lm }}</td>
					<td>{{ $product->name }}</td>
					<td>{{ $product->category }}</td>
					<td>{{ $product->present()->getFreeShipping }}</td>
					<td>{{ $product->description }}</td>
					<td>{{ $product->present()->getPrice }}</td>
					<td>
						<a href="{{ route('product.edit', ['id' => $product->id]) }}">
							Edit
						</a>
						|
						<a href="{{ route('product.destroy', ['id' => $product->id]) }}">
							Delete
						</a>
					</td>
				</tr>	
			@endforeach
		</tbody>
	</table>
@else
<p>No products to show.</p>
@endif
@endsection