<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/products', 'ProductController@index')->name('product.index');

Route::get('/product/create', 'ProductController@create')->name('product.create');

Route::get('/product/edit/{id}', 'ProductController@edit')->name('product.edit');

Route::get('/product/destroy/{id}', 'ProductController@destroy')->name('product.destroy');

Route::post('/product/update/{id}', 'ProductController@update')->name('product.update');

Route::post('/product/import', 'ProductController@import')->name('product.import');
