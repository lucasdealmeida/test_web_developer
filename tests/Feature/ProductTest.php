<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;
use App\Product;

class ProductTest extends TestCase
{
	public $mock;

	public function __construct()
	{
		$this->mock = Mockery::mock('App\ProductRepositoryInterface');
	}

	public function tearDown()
	{
       Mockery::close();
    }

    public function testViewProductsList()
    {
        $this->app->instance('App\ProductRepositoryInterface', $this->mock);
       
        $this->mock->shouldReceive('all')
            ->once(); 
       
        $this->get('products')
            ->assertViewHas('products');
    }

    public function testViewProduct()
    {
        $this->app->instance('App\ProductRepositoryInterface', $this->mock);
       
        $this->mock->shouldReceive('find')
            ->once()
            ->andReturn(new Product);
       
        $this->get('product/edit/1')
            ->assertViewHas('product');
    }
}
