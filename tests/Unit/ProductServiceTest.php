<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Services\Product as ProductService;
use Mockery;
use Illuminate\Support\Facades\Queue;
use App\Jobs\ImportProducts;

class ProductServiceTest extends TestCase
{
	public $productRepositoryMock;

    public $productFileMock;

    public $fileMock;

    protected function setUp()
    {
        $this->app = $this->createApplication();

        $this->productRepositoryMock = Mockery::mock('App\ProductRepositoryInterface');
        $this->app->instance('App\ProductRepositoryInterface', $this->productRepositoryMock);

        $this->productFileMock = Mockery::mock('App\ProductFile');
        $this->app->instance('App\ProductFile', $this->productFileMock);

        $this->fileMock = Mockery::mock('Illuminate\Http\UploadedFile');
        $this->app->instance('Illuminate\Http\UploadedFile', $this->fileMock);

        $this->productService = new ProductService($this->productRepositoryMock, $this->productFileMock);

    }

	public function tearDown()
	{
       Mockery::close();
    }

    public function testUpdateProduct()
    {
        $this->productRepositoryMock->shouldReceive('update')->once()->andReturn(true);
    	$this->assertTrue($this->productService->update([], 1));
    }

    public function testDestroyProduct()
    {
        $this->productRepositoryMock->shouldReceive('destroy')->once()->andReturn(true); 
        $this->assertTrue($this->productService->destroy(1));
    }

    public function testImportProduct(){
        $this->productRepositoryMock
            ->shouldReceive(['findByLm' => 1, 'create' => 1])
            ->andReturnValues([false, true]); 
        $this->productFileMock
            ->shouldReceive(['getProducts' => 1])
            ->andReturn([['lm' => 1]]);     
        
        $this->assertEmpty($this->productService->import(''));
    }

    public function testUploadFileProduct(){
        Queue::fake();

        $this->productFileMock
            ->shouldReceive(['validateFile' => 1]);    

        $this->fileMock
            ->shouldReceive(['path' => 1, 'store' => 1])
            ->andReturnValues(['test.xlsx', 'products/test.xlsx']);     
        
        $this->assertEmpty($this->productService->upload($this->fileMock));

        Queue::assertPushed(ImportProducts::class, function ($job){
            return $job->path === storage_path('app/test.xlsx');
        });
    }
}
